<?php

namespace CreatorsHub\OAuth2\Client\Test\Provider;

use CreatorsHub\OAuth2\Client\Provider\CreatorsHub;
use CreatorsHub\OAuth2\Client\Provider\CreatorsHubGroup;
use CreatorsHub\OAuth2\Client\Provider\CreatorsHubUser;
use Eloquent\Phony\Phpunit\Phony;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use CreatorsHub\OAuth2\Client\Provider\CreatorsHub as CreatorsHubProvider;
use League\OAuth2\Client\Token\AccessToken;

class CreatorsHubTest extends \PHPUnit_Framework_TestCase
{
    protected $provider;

    protected function setUp()
    {
        $this->provider = new CreatorsHubProvider([
            'clientId' => 'mock_client_id',
            'clientSecret' => 'mock_secret',
            'redirectUri' => 'none',
        ]);
    }

    public function testAuthorizationUrl()
    {
        $url = $this->provider->getAuthorizationUrl();
        $uri = parse_url($url);
        parse_str($uri['query'], $query);

        $this->assertArrayHasKey('client_id', $query);
        $this->assertArrayHasKey('redirect_uri', $query);
        $this->assertArrayHasKey('state', $query);
        $this->assertArrayHasKey('scope', $query);
        $this->assertArrayHasKey('response_type', $query);
    }

    public function testBaseAccessTokenUrl()
    {
        $url = $this->provider->getBaseAccessTokenUrl([]);
        $uri = parse_url($url);

        $this->assertEquals('/oauth/token/', $uri['path']);
    }

    public function testResourceOwnerDetailsUrl()
    {
        $token = $this->mockAccessToken();

        $url = $this->provider->getResourceOwnerDetailsUrl($token);
        $uri = parse_url($url);

        $this->assertEquals('/api/core/me', $uri['path']);
        $this->assertNotContains('mock_access_token', $url);
    }

    public function testUserData()
    {
        // Mock
        $response = json_decode('{"id": 39,"name": "CreatorsHub","title": "","formattedName": "<span style=\'color:#b70b0b\'>CreatorsHub<\/span>","primaryGroup": {"id": 1,"name": "Administrators","formattedName": "<span style=\'color:#b70b0b\'>Administrators<\/span>"},"joined": "2018-01-27T14:05:59Z","reputationPoints": 1,"photoUrl": "https:\/\/pbs.twimg.com\/profile_images\/957674656332484608\/zigGh59l_normal.jpg","photoUrlIsDefault": false,"coverPhotoUrl": "\/\/ams3.digitaloceanspaces.com\/creatorshub-community\/set_resources_2\/84c1e40ea0e759e3f1505eb1788ddf3c_pattern.png","profileUrl": "https:\/\/www.creatorshub.net\/profile\/39-creatorshub\/","posts": 8,"lastActivity": "2018-07-03T19:19:18Z","lastVisit": "2018-07-03T16:00:45Z","lastPost": "2018-07-03T14:28:59Z","profileViews": 193,"birthday": "1\/17\/1999","customFields": {},"email": "info@creatorshub.net"}', true);

        $token = $this->mockAccessToken();

        $provider = Phony::partialMock(CreatorsHubProvider::class);
        $provider->fetchResourceOwnerDetails->returns($response);
        $creatorshub = $provider->get();

        // Execute
        /** @var CreatorsHubUser $user */
        $user = $creatorshub->getResourceOwner($token);

        // Verify
        Phony::inOrder(
            $provider->fetchResourceOwnerDetails->called()
        );

        $this->assertInstanceOf('League\OAuth2\Client\Provider\ResourceOwnerInterface', $user);

        $this->assertEquals(39, $user->getId());
        $this->assertEquals('CreatorsHub', $user->getName());
        $this->assertNull($user->getTitle());
        $this->assertTrue($user->getPrimaryGroup() instanceof CreatorsHubGroup);
        $this->assertEquals('2018-01-27T14:05:59Z', $user->getJoinedDate());
        $this->assertEquals(1, $user->getReputationPoints());
        $this->assertEquals('https://pbs.twimg.com/profile_images/957674656332484608/zigGh59l_normal.jpg', $user->getPhotoUrl());
        $this->assertEquals('//ams3.digitaloceanspaces.com/creatorshub-community/set_resources_2/84c1e40ea0e759e3f1505eb1788ddf3c_pattern.png', $user->getCoverPhotoUrl());
        $this->assertEquals('https://www.creatorshub.net/profile/39-creatorshub/', $user->getProfileUrl());
        $this->assertEquals(8, $user->getPostCount());
        $this->assertEquals('2018-07-03T19:19:18Z', $user->getLastActivityDate());
        $this->assertEquals('2018-07-03T16:00:45Z', $user->getLastVisitDate());
        $this->assertEquals('2018-07-03T14:28:59Z', $user->getLastPostDate());
        $this->assertEquals(193, $user->getProfileViews());
        $this->assertEquals('1/17/1999', $user->getBirthday());
        $this->assertEquals('info@creatorshub.net', $user->getEmail());

        $this->assertEquals([
            'id' => 39,
            'name' => 'CreatorsHub',
            'title' => null,
            'primaryGroup' => [
                'id' => 1,
                'name' => 'Administrators'
            ],
            'joinedDate' => '2018-01-27T14:05:59Z',
            'reputationPoints' => 1,
            'photoUrl' => 'https://pbs.twimg.com/profile_images/957674656332484608/zigGh59l_normal.jpg',
            'coverPhotoUrl' => '//ams3.digitaloceanspaces.com/creatorshub-community/set_resources_2/84c1e40ea0e759e3f1505eb1788ddf3c_pattern.png',
            'profileUrl' => 'https://www.creatorshub.net/profile/39-creatorshub/',
            'postCount' => 8,
            'lastActivityDate' => '2018-07-03T19:19:18Z',
            'lastVisitDate' => '2018-07-03T16:00:45Z',
            'lastPostDate' => '2018-07-03T14:28:59Z',
            'profileViews' => 193,
            'birthday' => '1/17/1999',
            'email' => 'info@creatorshub.net'
        ], $user->toArray());
    }

    public function testErrorResponse()
    {
        // Mock
        $error_json = '{"error": {"code": 400, "message": "I am an error"}}';

        $response = Phony::mock('GuzzleHttp\Psr7\Response');
        $response->getHeader->returns(['application/json']);
        $response->getBody->returns($error_json);

        $provider = Phony::partialMock(CreatorsHubProvider::class);
        $provider->getResponse->returns($response);

        $creatorsHub = $provider->get();

        $token = $this->mockAccessToken();

        // Expect
        $this->expectException(IdentityProviderException::class);

        // Execute
        $user = $creatorsHub->getResourceOwner($token);

        // Verify
        Phony::inOrder(
            $provider->getResponse->calledWith($this->instanceOf('GuzzleHttp\Psr7\Request')),
            $response->getHeader->called(),
            $response->getBody->called()
        );
    }

    public function testUnusualErrorResponse()
    {
        // Mock
        $error_json = '{"error": "I am an error"}';

        $response = Phony::mock('GuzzleHttp\Psr7\Response');
        $response->getHeader->returns(['application/json']);
        $response->getBody->returns($error_json);

        $provider = Phony::partialMock(CreatorsHubProvider::class);
        $provider->getResponse->returns($response);

        $creatorshub = $provider->get();

        $token = $this->mockAccessToken();

        // Expect
        $this->expectException(IdentityProviderException::class);

        // Execute
        $user = $creatorshub->getResourceOwner($token);

        // Verify
        Phony::inOrder(
            $provider->getResponse->calledWith($this->instanceOf('GuzzleHttp\Psr7\Request')),
            $response->getHeader->called(),
            $response->getBody->called()
        );
    }
    
    /**
     * @return AccessToken
     */
    private function mockAccessToken()
    {
        return new AccessToken([
            'access_token' => 'mock_access_token',
        ]);
    }
}