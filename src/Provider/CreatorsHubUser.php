<?php

namespace CreatorsHub\OAuth2\Client\Provider;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

class CreatorsHubUser implements ResourceOwnerInterface
{
    /**
     * @var array
     */
    protected $response;

    /**
     * @param array $response
     */
    public function __construct(array $response)
    {
        $this->response = $response;
    }

    /**
     * Get user ID
     *
     * @return string|null
     */
    public function getId()
    {
        return $this->response['id'] ?: null;
    }

    /**
     * Get user name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->response['name'] ?: null;
    }

    /**
     * Get user's member title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->response['title'] ?: null;
    }

    /**
     * Get user's primary group
     *
     * @return CreatorsHubGroup|null
     */
    public function getPrimaryGroup()
    {
        return new CreatorsHubGroup($this->response['primaryGroup'] ?: []);
    }

    /**
     * Get user's registration date
     *
     * @return string|null
     */
    public function getJoinedDate()
    {
        return $this->response['joined'] ?: null;
    }

    /**
     * Get user's number of reputation points
     *
     * @return int|null
     */
    public function getReputationPoints()
    {
        return $this->response['reputationPoints'] ?: null;
    }

    /**
     * Get user's avatar url
     *
     * @return string|null
     */
    public function getPhotoUrl()
    {
        return $this->response['photoUrl'] ?: null;
    }

    /**
     * Get user's cover photo url
     *
     * @return string|null
     */
    public function getCoverPhotoUrl()
    {
        return $this->response['coverPhotoUrl'] ?: null;
    }

    /**
     * Get user's profile url
     *
     * @return string|null
     */
    public function getProfileUrl()
    {
        return $this->response['profileUrl'] ?: null;
    }

    /**
     * Get user's post count
     *
     * @return int|null
     */
    public function getPostCount()
    {
        return $this->response['posts'] ?: null;
    }

    /**
     * Get user's last activity date
     *
     * @return string|null
     */
    public function getLastActivityDate()
    {
        return $this->response['lastActivity'] ?: null;
    }

    /**
     * Get user's last forum visit date
     *
     * @return string|null
     */
    public function getLastVisitDate()
    {
        return $this->response['lastVisit'] ?: null;
    }

    /**
     * Get user's last post date
     *
     * @return string|null
     */
    public function getLastPostDate()
    {
        return $this->response['lastPost'] ?: null;
    }

    /**
     * Get user's number of profile views
     *
     * @return int|null
     */
    public function getProfileViews()
    {
        return $this->response['profileViews'] ?: null;
    }

    /**
     * Get user's date of birth
     *
     * @return string|null
     */
    public function getBirthday()
    {
        return $this->response['birthday'] ?: null;
    }

    /**
     * Get user's email
     *
     * @return int|null
     */
    public function getEmail()
    {
        return $this->response['email'] ?: null;
    }

    /**
     * Get user data as an array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'title' => $this->getTitle(),
            'primaryGroup' => $this->getPrimaryGroup()->toArray(),
            'joinedDate' => $this->getJoinedDate(),
            'reputationPoints' => $this->getReputationPoints(),
            'photoUrl' => $this->getPhotoUrl(),
            'coverPhotoUrl' => $this->getCoverPhotoUrl(),
            'profileUrl' => $this->getProfileUrl(),
            'postCount' => $this->getPostCount(),
            'lastActivityDate' => $this->getLastActivityDate(),
            'lastVisitDate' => $this->getLastVisitDate(),
            'lastPostDate' => $this->getLastPostDate(),
            'profileViews' => $this->getProfileViews(),
            'birthday' => $this->getBirthday(),
            'email' => $this->getEmail(),
        ];
    }
}
