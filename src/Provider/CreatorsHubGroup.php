<?php

namespace CreatorsHub\OAuth2\Client\Provider;

class CreatorsHubGroup
{
    /**
     * @var array
     */
    protected $group;

    /**
     * @param array $group
     */
    public function __construct(array $group)
    {
        $this->group = $group;
    }

    /**
     * Get forum group ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->group['id'] ?: null;
    }

    /**
     * Get forum group name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->group['name'] ?: null;
    }

    /**
     * Get user data as an array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName()
        ];
    }
}
